;; init.el - my emacs configuration
;; Copyright (C) 2022 João Pedro de O. Simas

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(bongo-default-directory "~/Music/")
 '(bongo-enabled-backends '(mpv))
 '(column-number-mode t)
 '(custom-enabled-themes '(tango-dark-custom))
 '(custom-safe-themes
   '("5c9b6fe7d0f3856471237e947d5bef5ac7d8ec83aab7c88a00e01ddaf4bc9006" "18d0957a0644af30c064cff708acf3da9790ae22c51eff0c6c9f0a297db83ff5" "3018be254d3c1d72a7e09f49f773845271ed303e31d79d9e9c493994d742f02b" "5cb5e11c8d82785f4c40b1c26f6794d3bf0fcd5c5fd4187688d64bdd7e8e04ee" "f24d2657942ad401ef49017af2d6c6128d086df639938930ae241fda168c664a" "d1fab9efd6ee0029fd7bdbab048c16e3f34a1419bf172e20bf77f63120f093d4" "f431f7b0c56a95e3f8affb03a687601ac827e298c5dcaab6305227d8d716b888" "d43143f971547baaea62604ebdfbcadd511041647bb5bd663746290c103fcc56" "e7293ad2369bfa1871e5f819f391f683e207d26ea7c57d6029cbcfe2fd0b9a43" "8362719e6623484719feaeb2419cf8e13be84231a5f0a40fd81df5f687903648" "a50b3ffeda0fe2724df56f9e959591f91368d259d865a123e884b1f41722c258" "0ff9fd2347c1a6635943827259961f2473ba1a2013b4b06031c039ab05b0b40e" "7acbd85aa0fe12b57041632336911b41511a4d5178edbd65ff7ccb779ccce1f8" "ec5271a02d7e5778735e07d5e1c4e3124a2e5996a4224b5777d151d86691278b" "0db60353d2382238f55464fca1362cb51ef06d289e61bc2ea31061459c259ce4" "abe62ce0b9cc6434cf6b26de33c26cc239d1099ca34c478b34c71ae5ef781079" "24ac983cebf30c36c5f2389bef7de61516971172c81cd13cad4f4b82f3ecc1bf" "599efd02edca25891528c5cf84a4117878550a2eb78801264dfe92ce00f35bc7" "1cdb166e4d70e8c78a54350fc8846aafdd23fc8dc4f06d023c7225a54077a5d4" "a9e4be70bdbf914668127794b41b754dc4b07127e31053f3b48073021634c5c1" "db374801becfeb8f7c628d8815b0f586487adbd3e49220d86c51583f79acdff9" "9ef9559c72b6e63302c2419ebe8df399e0e752eccdc00851490b799ca226cd13" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" "d4673d1ef268ad4e63cbfc9d3d15222233b82585724a3ba63887f067e9b580d9" "83cc4b18586669e5fcf32c338f2977ad83ff7c114733e45e9eb7d9c867bd9aeb" "e1f0a9c632c50e111350190a95f7a397032edf38438f9a87c10eef3c9dc9e71e" "ac9175e3355015751809f61fc64477ea2ea16de20f172528154bad02654d1df5" "1bcc0401cc30ff7c5de08caf627405232bd9e4ae8422da7b71d753e0a78eb81d" "3bce2b4f73bf2056bf6c93fe2de4a7defa4e2216292a64c0f406b949b9eb8935" "edbd3a4752e8471a74220bc9b5d61645717d21e1cbc77cce57382ce7adf88511" "3a4044fd2ed5ddae97d3d3a43956d07793cc8c852fb6a767f3cf319811213173" "a2360306ea2abcec39ea7e66f4c50ade372964adca67b11c57b1aa6d13bdaf59" "f735764d7813a1cb2462b42a99572a830a6ebd09971944e4571f12e0a8877d78" "cbae1ac27e21ae4350e166ec896b112243b3ae3cfa809b1b50d0e42b1376d0c7" "95d0ed21bb0e919be7687a25ad59a1c2c8df78cbe98c9e369d44e65bfd65b167" "1623aa627fecd5877246f48199b8e2856647c99c6acdab506173f9bb8b0a41ac" "6c3b5f4391572c4176908bb30eddc1718344b8eaff50e162e36f271f6de015ca" "aaa4c36ce00e572784d424554dcc9641c82d1155370770e231e10c649b59a074" "7b3d184d2955990e4df1162aeff6bfb4e1c3e822368f0359e15e2974235d9fa8" "7546a14373f1f2da6896830e7a73674ef274b3da313f8a2c4a79842e8a93953e" "c83c095dd01cde64b631fb0fe5980587deec3834dc55144a6e78ff91ebc80b19" "730a87ed3dc2bf318f3ea3626ce21fb054cd3a1471dcd59c81a4071df02cb601" "2cdc13ef8c76a22daa0f46370011f54e79bae00d5736340a5ddfe656a767fddf" "7d708f0168f54b90fc91692811263c995bebb9f68b8b7525d0e2200da9bc903c" "4bca89c1004e24981c840d3a32755bf859a6910c65b829d9441814000cf6c3d0" "8f5a7a9a3c510ef9cbb88e600c0b4c53cdcdb502cfe3eb50040b7e13c6f4e78e" "c4bdbbd52c8e07112d1bfd00fee22bf0f25e727e95623ecb20c4fa098b74c1bd" "76bfa9318742342233d8b0b42e824130b3a50dcc732866ff8e47366aed69de11" "e1ef2d5b8091f4953fe17b4ca3dd143d476c106e221d92ded38614266cea3c8b" "6b80b5b0762a814c62ce858e9d72745a05dd5fc66f821a1c5023b4f2a76bc910" "0e2a7e1e632dd38a8e0227d2227cb8849f877dd878afb8219cb6bcdd02068a52" "fce3524887a0994f8b9b047aef9cc4cc017c5a93a5fb1f84d300391fba313743" "c086fe46209696a2d01752c0216ed72fd6faeabaaaa40db9fc1518abebaf700d" "fb941dde69c278b9318fce502550893964ced8b12511a25e640703bccf26bfec" default))
 '(display-time-24hr-format t)
 '(display-time-day-and-date t)
 '(display-time-default-load-average nil)
 '(display-time-mode t)
 '(elfeed-feeds '("https://xadrezverbal.com/feed/"))
 '(elfeed-search-filter '("@6-months-ago"))
 '(eradio-player '("mpv" "--no-video" "--no-terminal"))
 '(fci-rule-color "#4F4F4F")
 '(fringe-mode '(4 . 4) nil (fringe))
 '(helm-completion-style 'emacs)
 '(inhibit-startup-screen t)
 '(jdee-db-active-breakpoint-face-colors (cons "#000000" "#8CD0D3"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#000000" "#7F9F7F"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#000000" "#494949"))
 '(menu-bar-mode nil)
 '(objed-cursor-color "#CC9393")
 '(overflow-newline-into-fringe t)
 '(package-selected-packages
   '(ein nix-mode speed-type nhexl-mode auctex multi-vterm vterm elfeed code-cells eradio bongo frameshot dim which-key company volume ace-window slime-repl resize-window pdf-tools bison-mode rust-mode lorem-ipsum org-ref yaml-mode slime function-args wc-mode w3m typing transmission toc-org spice-mode scad-preview portage-navi pacmacs org-plus-contrib org-bullets org magit helm-chronos helm-chrome gnuplot gnugo exwm emms-player-mpv csv-mode chess arduino-mode anzu))
 '(pdf-view-midnight-colors (cons "#DCDCDC" "#3F3F3F"))
 '(python-shell-interpreter "python3")
 '(rustic-ansi-faces
   ["#3F3F3F" "#CC9393" "#7F9F7F" "#F0DFAF" "#8CD0D3" "#DC8CC3" "#93E0E3" "#DCDCDC"])
 '(scroll-bar-mode nil)
 '(sml/col-number-format "%3c")
 '(sml/line-number-format "%3l")
 '(sml/modified-char "*")
 '(tool-bar-mode nil)
 '(vc-annotate-background "#3F3F3F")
 '(vc-annotate-color-map
   (list
    (cons 20 "#7F9F7F")
    (cons 40 "#a4b48f")
    (cons 60 "#cac99f")
    (cons 80 "#F0DFAF")
    (cons 100 "#eacfa4")
    (cons 120 "#e4bf99")
    (cons 140 "#DFAF8F")
    (cons 160 "#dea3a0")
    (cons 180 "#dd97b1")
    (cons 200 "#DC8CC3")
    (cons 220 "#d68eb3")
    (cons 240 "#d190a3")
    (cons 260 "#CC9393")
    (cons 280 "#ab8080")
    (cons 300 "#8a6d6d")
    (cons 320 "#695b5b")
    (cons 340 "#4F4F4F")
    (cons 360 "#4F4F4F")))
 '(vc-annotate-very-old-color nil)
 '(visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(scroll-bar ((t nil)))
 '(sml/filename ((t (:inherit sml/global :foreground "#ffffff" :weight bold))))
 '(sml/prefix ((t (:inherit sml/global :foreground "#73d216")))))

;;resize-window shorcut
(global-set-key (kbd "C-x r") 'resize-window)

;;Y or N
(defalias 'yes-or-no-p 'y-or-n-p)

;;Guix Packages
(if (require 'guix nil t)
(add-to-list 'load-path "~/.guix-profile/share/emacs/site-lisp/")
(if (fboundp 'guix-emacs-autoload-packages)
    (guix-emacs-autoload-packages)))

;;ELPA Repositories
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ;;("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
			 ("org" . "http://orgmode.org/elpa/")))
(require 'package)
(package-initialize)

;;printing
(require 'printing)

;;Org Mode
;;toc-org
(if (require 'toc-org nil t)
    (add-hook 'org-mode-hook 'toc-org-enable)
  (warn "toc-org not found"))

;; make org export run asynchronously
(setq org-export-in-background t)

;; org capture file
(setq org-default-notes-file (concat org-directory "/notes.org"))

;;which-key
(which-key-mode)

;;HELM
(require 'helm-config)
(helm-mode t)

;;Org Bullets
(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;;Reenabled Stuff
(put 'erase-buffer 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)

;;EMMS
;; (require 'emms-setup)
;; (emms-standard)
;; (emms-default-players)
;; (setq emms-is-playing 0)
;; (defun emms-toggle-play ()
;;   (interactive)
;;   (if (= emms-is-playing 1)
;;       (progn (emms-pause)
;;        (setq emms-is-playing 0))
;;     (progn (emms-start)
;;      (setq emms-is-playing 1))))
			    
;; (define-emms-simple-player mplayer '(file url)
;;   (regexp-opt '(".ogg" ".mp3" ".wav" ".mpg" ".mpeg" ".wmv" ".wma"
;; 		".mov" ".avi" ".divx" ".ogm" ".asf" ".mkv" "http://" "mms://"
;; 		".rm" ".rmvb" ".mp4" ".flac" ".vob" ".m4a" ".flv" ".ogv" ".pls"))
;;   "mplayer" "-slave" "-quiet" "-really-quiet" "-fullscreen")
;; (global-set-key (kbd "<XF86AudioRaiseVolume>") 'emms-volume-raise)
;; (global-set-key (kbd "<XF86AudioLowerVolume>") 'emms-volume-lower)
;; (global-set-key (kbd "<XF86AudioNext>") 'emms-next)
;; (global-set-key (kbd "<XF86AudioPrev>") 'emms-previous)
;; (global-set-key (kbd "<XF86AudioPlay>") 'emms-toggle-play)
;; (global-set-key (kbd "<XF86AudioStop>") 'emms-stop)

;;Octave Mode
(add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode))

;;Set default encoding to UTF8
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
;; backwards compatibility as default-buffer-file-coding-system
;; is deprecated in 23.2.
(if (boundp 'buffer-file-coding-system)
    (setq-default buffer-file-coding-system 'utf-8)
  (setq default-buffer-file-coding-system 'utf-8))
;; Treat clipboard input as UTF-8 string first; compound text next, etc.
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

;;Anzu
(global-anzu-mode +1)

(add-to-list 'org-latex-packages-alist '("" "listings" nil))
(setq org-latex-listings t)
(setq org-latex-listings-options '(("breaklines" "true")))

;;function-args
(fa-config-default)

;;SLIME (with SBCL)
(setq inferior-lisp-program "sbcl")
(require 'slime)
(slime-setup '(slime-fancy slime-tramp slime-asdf))
(slime-require :swank-listener-hooks)

;;org-ref
(setq reftex-default-bibliography '("~/.org-ref/references.bib"))

;; see org-ref for use of these variables
;; (setq org-ref-bibliography-notes "~/.org-ref/notes.org"
;;       org-ref-default-bibliography '("~/.org-ref/bibliography/references.bib")
;;       org-ref-pdf-directory "~/.org-ref/bibliography/bibtex-pdfs/")

(setq org-latex-pdf-process (list "latexmk -shell-escape -bibtex -f -pdf %f"))

;; (setq bibtex-completion-bibliography "~/.org-ref/bibliography/references.bib"
;;       bibtex-completion-library-path "~/.org-ref/bibliography/bibtex-pdfs"
;;       bibtex-completion-notes-path "~/.org-ref/bibliography/helm-bibtex-notes")

;; open pdf with system pdf viewer (works on mac)
(setq bibtex-completion-pdf-open-function
  (lambda (fpath)
    (start-process "open" "*open*" "open" fpath)))

(require 'org-ref)

;;otave mode use % for comments
(add-hook 'octave-mode-hook
    (lambda () (progn (setq octave-comment-char ?%)
                      (setq comment-start "%%")
                      (setq comment-add 0))))

;;org enable shell exec
(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (shell . t)
   )
 )

;;ditaa org export
(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (ditaa . t)
   )
 )

(set 'org-ditaa-jar-path "/usr/share/java/ditaa/ditaa-0.11.jar")

;;ditaa octave export
(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (octave . t)
   )
 )

;;ox-extra
(require 'ox-extra)
(ox-extras-activate '(ignore-headlines))


;;MU4E
(if (require 'mu4e nil t)

    ;;set emacs' email variables
    (setq user-full-name "João Pedro de Omena Simas")
  (setq user-mail-address "joao.simas@usp.br")

  ;; SMTP settings:
  (setq send-mail-function 'smtpmail-send-it)    ; should not be modified
  (setq smtpmail-smtp-server "smtp.gmail.com") ; host running SMTP server
  (setq smtpmail-smtp-service 587)               ; SMTP service port number
  (setq smtpmail-stream-type 'starttls)          ; type of SMTP connections to use

  ;; Mail folders:
  (setq mu4e-drafts-folder "/Drafts")
  (setq mu4e-sent-folder   "/Sent")
  (setq mu4e-trash-folder  "/Trash")

  ;; The command used to get your emails (adapt this line, see section 2.3):
  (setq mu4e-get-mail-command "mbsync --config ~/.emacs.d/.mbsyncrc usp")
  ;; Further customization:
  (setq mu4e-html2text-command "w3m -T text/html" ; how to hanfle html-formatted emails
	mu4e-update-interval 300                  ; seconds between each mail retrieval
	mu4e-headers-auto-update t                ; avoid to type `g' to update
	mu4e-view-show-images t                   ; show images in the view buffer
	mu4e-compose-signature-auto-include nil   ; I don't want a message signature
	mu4e-use-fancy-chars t)                   ; allow fancy icons for mail threads
)

;; Ace Window
;; set scope to frame instead of global
(setq aw-scope 'frame)

;; Key Bindings
;; resize window
(global-set-key (kbd "C-x r") 'resize-window)

;; Ace Window
(global-set-key (kbd "C-x o") 'ace-window)

;; HELM
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x C-f") #'helm-find-files)
(global-unset-key (kbd "C-x b"))
(global-set-key (kbd "C-x b") 'helm-buffers-list)
(global-unset-key (kbd "C-x d"))
(global-set-key (kbd "C-x d") 'dired)

;; Comment Region
(global-set-key (kbd "C-c C-c") 'comment-or-uncomment-region)

;; Company Mode
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)
(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))

;; Disable beep
(setq visible-bell t)

;; Mode line display time on the right of the mode line
(display-time)
(setq global-mode-string (remove 'display-time-string
				 global-mode-string))
(setq mode-line-end-spaces
      (list (propertize " " 'display '(space :align-to (- right (+ 16 1))))
            'display-time-string))

;; Rename modes with dim
(when (require 'dim nil t)
  (dim-major-names
   '())
  (dim-minor-names
   '((company-mode       " Cpny"   company)
     (eldoc-mode         " ElD"    eldoc)
     (anzu-mode          " Anz"    anzu)
     (helm-mode          " Hlm"    helm)
     (abbrev-mode        " Abbr"   abbrev))))

;; Activate PDF Tools
(pdf-loader-install)

;; GPG pin entry on minibuffer
(setq epg-pinentry-mode 'loopback)

;; volume
(require 'volume)

;; Bongo Pause Resume Media Key
(global-set-key (kbd "<XF86AudioPlay>") 'bongo-pause/resume)
(global-set-key (kbd "<XF86AudioStop>") 'bongo-stop)
(global-set-key (kbd "<XF86AudioNext>") 'bongo-next)
(global-set-key (kbd "<XF86AudioPrev>") 'bongo-previous)
(global-set-key (kbd "<XF86AudioRaiseVolume>") 'volume-raise)
(global-set-key (kbd "<XF86AudioLowerVolume>") 'volume-lower)
(setq volume-last-volume 100)
(global-set-key (kbd "<XF86AudioMute>") '(lambda () (interactive)
					   (let ((curr-volume (volume-get)))
					     (progn
					       (if (= curr-volume 0)
						   (volume-set volume-last-volume)
						 (progn
						   (setq volume-last-volume curr-volume)
						   (volume-set 0)))))))

;;eradio
(setq eradio-channels '(("def con - soma fm" . "https://somafm.com/defcon256.pls")))

;;use aspell instead of ispell
(setq ispell-program-name "aspell")

;;org-babel python
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)))

(setq org-babel-python-command "python3")

;;dired human-readable sizes
(setq dired-listing-switches "-alh")

;; Emacs iPython Notebook settings
(setq ein:jupyter-default-server-command "jupyter-notebook")
(setq ein:jupyter-server-use-subcommand "")
