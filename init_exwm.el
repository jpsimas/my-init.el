;; init-exwm.el - my emacs configuration to be run when using exwm
;; Copyright (C) 2022 João Pedro de O. Simas

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; My EXWM Config
(require 'exwm)
(require 'exwm-config)

(defun my-exwm-config ()
  "Default configuration of EXWM."
  ;; Set the initial workspace number.
  (unless (get 'exwm-workspace-number 'saved-value)
    (setq exwm-workspace-number 4))
  ;; Make class name the buffer name
  (add-hook 'exwm-update-class-hook
            (lambda ()
              (exwm-workspace-rename-buffer exwm-class-name)))
  ;; Global keybindings.
  (unless (get 'exwm-input-global-keys 'saved-value)
    (setq exwm-input-global-keys
          `(
            ;; 's-r': Reset (to line-mode).
            ([?\s-r] . exwm-reset)
            ;; 's-w': Switch workspace.
            ([?\s-w] . exwm-workspace-switch)
            ;; 's-&': Launch application.
            ([?\s-&] . (lambda (command)
                         (interactive (list (read-shell-command "$ ")))
                         (start-process-shell-command command nil command)))
            ;; 's-N': Switch to certain workspace.
            ,@(mapcar (lambda (i)
                        `(,(kbd (format "s-%d" i)) .
                          (lambda ()
                            (interactive)
                            (exwm-workspace-switch-create ,i))))
                      (number-sequence 0 9))
	    )))
  ;; Line-editing shortcuts
  (unless (get 'exwm-input-simulation-keys 'saved-value)
    (setq exwm-input-simulation-keys
          '(([?\C-b] . [left])
            ([?\C-f] . [right])
            ([?\C-p] . [up])
            ([?\C-n] . [down])
            ([?\C-a] . [home])
            ([?\C-e] . [end])
            ([?\M-v] . [prior])
            ([?\C-v] . [next])
            ([?\C-d] . [delete])
            ([?\C-k] . [S-end delete]))))
	    
  ;; Enable EXWM
  (exwm-enable)
  ;; Other configurations
  (exwm-config-misc))

(my-exwm-config)
(require 'exwm-systemtray)
(exwm-systemtray-enable)

;; start lockcreen
;; (start-process-shell-command "i3lock" nil "xss-lock -- i3lock-fancy")
(start-process-shell-command "xlock" nil "xss-lock -- xlock +resetsaver")

;; set keyboard layout
(start-process-shell-command "setxkbmap" nil "setxkbmap -layout us -variant intl")

;; set cursor style
(start-process-shell-command "xsetroot" nil "xsetroot -cursor_name left_ptr")

;; set volume
(start-process-shell-command "amixer" nil "amixer -c PCH set Master 43%")

;; start  network manager applet (nm-applet)
(start-process-shell-command "nm-applet" nil "nm-applet > /dev/null 2>&1")

;; start compositor (picom)
(start-process-shell-command "picom" nil "picom -b --vsync --backend=glx")
;; (start-process-shell-command "picom" nil "picom -b --unredir-if-possible --backend xr_glx_hybrid --vsync --use-damage --glx-no-stencil")

;; start pulseaudio daemon
(start-process-shell-command "pulseaudio" nil "pulseaudio --start")

;; start server
(if (not (server-running-p))
    (server-start))

(defun local-lock ()
  (interactive)
  ;; (start-process-shell-command "i3lock-fancy" nil "i3lock-fancy -n"))
  (start-process-shell-command "xlock" nil "xlock +resetsaver"))

(defun local-suspend ()
  (interactive)
  (if (executable-find "systemctl")
      (shell-command "systemctl suspend")
    (shell-command "loginctl suspend")))

(defun local-poweroff ()
  (interactive)
  (if (executable-find "systemctl")
      (shell-command "systemctl poweroff")
    (shell-command "loginctl poweroff")))

(defun local-reboot ()
  (interactive)
  (if (executable-find "systemctl")
      (shell-command "systemctl reboot")
  (shell-command "loginctl reboot")))

(defun local-hibernate ()
  (interactive)
  (if (executable-find "systemctl")
      (shell-command "systemctl hibernate")
    (shell-command "loginctl hibernate")))

(global-set-key (kbd "C-s-p s") 'local-suspend)
(global-set-key (kbd "C-s-p l") 'local-lock)
(global-set-key (kbd "C-s-p o") 'local-poweroff)
(global-set-key (kbd "C-s-p b") 'local-reboot)
(global-set-key (kbd "C-s-p h") 'local-hibernate)
